package com.example.lab01

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnVerify.setOnClickListener {
            val age = txtYear.text.toString().toInt();
            txtResult.text = if(age >= 18) "Verficado! Puede Continuar" else "No Permitido!";
        }
    }
}